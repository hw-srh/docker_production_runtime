# System requirements:

* linux
* bash >=4.0 (soft requirement, you can just run docker-compose without the script)
* docker-compose >= 1.27
* docker >= 18.06.0
* internet access (for bitbucket repo and dockerhub images)

# How to get started:

### build the docker images and start the project

`chmod +x ./run.sh && ./run.sh --up`

### stop the docker containers

`./run.sh --down`

### update the web project
`./run.sh --update`

