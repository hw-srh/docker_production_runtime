#!/usr/bin/env bash

set -o pipefail # trace errors through pipes
set -o errtrace # trigger error in error trap even if the error occurs in a subshell
set -o nounset  # exit the script if an uninitialized variable is used
set -o errexit  # exit the script if a statement returns an exit code other than 0

function check_dependencies() {

  declare -A required_dependencies=(
    [docker]='Missing docker'
    ['docker-compose']='Missing docker-compose'
  )

  for dependency_name in "${!required_dependencies[@]}"; do
    if ! command -v "${dependency_name}" &>/dev/null; then
      echo -e "ERROR\n"
      echo "${required_dependencies[${dependency_name}]}"
      exit 1
    fi
  done
}

function get_path_to_script() {
  realpath "$(dirname "$0")"
}

function get_shared_docker_compose_command_prefix() {
  local script_dir="$(get_path_to_script)"

  echo "docker-compose \
        --file ${script_dir}/docker-compose.yml \
        --project-directory ${script_dir} \
        --env-file ${script_dir}/.env \
        --compatibility"
}

function start_docker_setup() {
  eval "$(get_shared_docker_compose_command_prefix) up --build --force-recreate --detach"
}

function stop_docker_setup() {
  eval "$(get_shared_docker_compose_command_prefix) down"
}

function rebuild_node_image() {
  eval "$(get_shared_docker_compose_command_prefix) build --pull --no-cache node"
}

function handle_flags() {

  case "${1:-}" in
  --up)
    start_docker_setup
    ;;
  --down)
    stop_docker_setup
    ;;
  --update)
    rebuild_node_image
    ;;
  *)
    false
    ;;
  esac
}

function print_help_message() {
  echo "./run.sh --up                                  builds the base images and starts the docker setup (will also start during system startup)"
  echo "./run.sh --down                                stops the docker setup (will no longer start during system startup)"
  echo "./run.sh --update                              update web project"
}

function check_if_docker_is_running() {
  if ! docker info &>/dev/null; then
    echo "Docker is not running!"
    exit 1
  fi
}

function main() {
  check_dependencies
  check_if_docker_is_running

  handle_flags "${@}" && exit 0

  print_help_message
}

main "${@}"
