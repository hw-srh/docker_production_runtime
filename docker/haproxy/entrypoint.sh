#!/usr/bin/env sh

set -o nounset ## exit the script if an uninitialized variable is used
set -o errexit ## exit the script if a statement returns an exit code other than 0

update_number_of_node_container_routes() {

    cat "/usr/local/etc/haproxy/template.haproxy.cfg" >/usr/local/etc/haproxy/main.haproxy.cfg
    echo -e "\n" >>/usr/local/etc/haproxy/main.haproxy.cfg

    for node_server_number in $(seq 1 "${NUMBER_OF_NODE_HTTP_SERVERS}"); do
        echo "    server node-server-${node_server_number} ${NODE_HTTP_NETWORK_PREFIX}.$((node_server_number + 1)):8000 \
        weight 1 maxconn 2000" >> /usr/local/etc/haproxy/main.haproxy.cfg
    done
}

construct_pem_full_chain() {
    echo -e "${KEY_PEM}\n${CERT_PEM}" > /usr/local/etc/haproxy/tls/fullchain.pem
}

update_permissions() {
    chown root:no_root /usr/local/etc/haproxy/main.haproxy.cfg
    chown root:no_root /usr/local/etc/haproxy/tls/fullchain.pem

    chmod 0640 /usr/local/etc/haproxy/main.haproxy.cfg
    chmod 0640 /usr/local/etc/haproxy/tls/fullchain.pem

    chmod 0750 /usr/local/etc/haproxy/tls
    chmod 0640 /usr/local/etc/haproxy/tls/* || true
}

main() {
    update_number_of_node_container_routes
    construct_pem_full_chain
    update_permissions

    exec su no_root -c "exec /docker-entrypoint.sh /usr/local/sbin/haproxy -f /usr/local/etc/haproxy/main.haproxy.cfg"
}

main "${@}"
