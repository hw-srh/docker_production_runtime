#!/bin/sh

set -o nounset ## exit the script if an uninitialized variable is used
set -o errexit ## exit the script if a statement returns an exit code other than 0

pid_of_last_background_process=""

shutdown_handler() {
  trap - INT TERM                                # remove old traps
  kill -TERM "${pid_of_last_background_process}" # send signal to background process
  wait "${pid_of_last_background_process}"
}

wait_for_mysql_server() {
  until mysqladmin ping -u"${DB_USERNAME}" -p"${DB_PASSWORD}" -h "${DB_HOST}" -P "${DB_PORT}" --silent; do
    sleep 1
    counter=$((counter + 1))
    if [ "${counter}" -gt "300" ]; then
      echo "No connection to ${DB_HOST} on port ${DB_PORT} within the last 5 minutes"
      exit 1
    fi
  done
}

run_startup_queries() {
  # sed is used to replace the crypt_blowfish version prefix with the openbsd version, they are compatible but use different prefixes
  admin_password_hash="$(htpasswd -bnBC "${DEFAULT_SALT_ROUNDS}" "" "${DEFAULT_SYSTEM_ADMIN_PASSWORD}" | tr -d ':\n' | sed 's/$2y/$2a/')"

  mysql --connect_timeout=60 -u"${DB_USERNAME}" -p"${DB_PASSWORD}" -h "${DB_HOST}" -P "${DB_PORT}" -D "${DB_DEFAULT_DATABASE_NAME}" -e \
    "set @admin_password_hash='${admin_password_hash}';\
    set @admin_email='${DEFAULT_SYSTEM_ADMIN_EMAIL}';\
    set @admin_username='${DEFAULT_SYSTEM_ADMIN_USERNAME:-${DEFAULT_SYSTEM_ADMIN_EMAIL}}';\
    set @admin_firstname='${DEFAULT_SYSTEM_ADMIN_FIRSTNAME}';\
    set @admin_lastname='${DEFAULT_SYSTEM_ADMIN_LASTNAME}';\
    DELETE FROM password_reset_token; ALTER TABLE password_reset_token AUTO_INCREMENT = 1;\
    INSERT INTO user (id, email, username, password, firstname, lastname, work_hours_per_week) \
    VALUES(1, @admin_email, @admin_username, @admin_password_hash, @admin_firstname, @admin_lastname, 40) ON DUPLICATE KEY UPDATE \
    email = @admin_email, username = @admin_username, password = @admin_password_hash, firstname = @admin_firstname, lastname = @admin_lastname;" ||
    exit 1

  mysql --connect_timeout=60 -u"${DB_USERNAME}" -p"${DB_PASSWORD}" -h "${DB_HOST}" -P "${DB_PORT}" -D "${DB_DEFAULT_DATABASE_NAME}" -e \
    "set @admin_email='${DEFAULT_SYSTEM_ADMIN_EMAIL}'; \
    set @user_id=(SELECT id from user WHERE email = @admin_email); \
    INSERT INTO user_role VALUES(@user_id, 'admin') \
    ON DUPLICATE KEY UPDATE user_id =  @user_id, role_id = 'admin';" ||
    exit 1
}

main() {
  wait_for_mysql_server
  run_startup_queries

  echo "setup script completed successfully !"

  trap shutdown_handler TERM INT

  # to avoid having to use systemd we simply run
  # this container with the restart always option and
  # block indefinitely after the startup scripts have been executed
  sleep infinity &

  pid_of_last_background_process="${!}"
  wait "${pid_of_last_background_process}"
}

main "${@}"
