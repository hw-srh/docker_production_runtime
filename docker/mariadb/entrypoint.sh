#!/usr/bin/env bash

set -o pipefail # trace errors through pipes
set -o errtrace # trigger error in error trap even if the error occurs in a subshell
set -o nounset  # exit the script if an uninitialized variable is used
set -o errexit  # exit the script if a statement returns an exit code other than 0

set_permissions(){
  chown root:mysql /docker-entrypoint-initdb.d/*
  chmod 0640 /docker-entrypoint-initdb.d/*
}

get_query_string_for_root_user_remote_access(){

    if [ "${APP_ENV:-}" != "dev" ] && [ "${APP_ENV:-}" != "development" ]
    then
      echo "DROP USER 'root'@'%';"
    else
      echo "#DROP USER 'root'@'%';"
    fi

}

overwrite_placeholders_with_env_variables(){
  if [ ! -f /docker-entrypoint-initdb.d/z_privileges.sql ]
  then
    \cp -f /default_db_init_scripts/* /docker-entrypoint-initdb.d/
    
    cat /default_db_init_scripts/z_privileges.sql | \
    awk '{ gsub(/DROP USER '\''root'\''@'\''%'\'';/, replacement_str); print}' replacement_str="$(get_query_string_for_root_user_remote_access)" | \
    awk '{ gsub(/<PASSWORD_PLACEHOLDER_ADMIN>/, admin_password); print}' admin_password="${DB_ADMIN_PASSWORD}" | \
    awk '{ gsub(/<INTERNAL_NETWORK_PREFIX>/, network_prefix); print}' network_prefix="${INTERNAL_NETWORK_PREFIX}" | \
    awk '{ gsub(/<EXTERNAL_NETWORK_PREFIX>/, network_prefix); print}' network_prefix="${EXTERNAL_NETWORK_PREFIX}" > \
    /docker-entrypoint-initdb.d/z_privileges.sql
  fi
}

overwrite_placeholders_with_env_variables
set_permissions

exec su mysql -c "exec /usr/local/bin/docker-entrypoint.sh \"mysqld\""