CREATE DATABASE IF NOT EXISTS system CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

use system;

CREATE TABLE `user`
(
    `id`                         int unsigned       NOT NULL auto_increment PRIMARY KEY,
    `email`                      varchar(255)       NOT NULL,
    `username`                   varchar(255)       NOT NULL,
    `password`                   varchar(255)       NOT NULL,
    `firstname`                  varchar(255)       NOT NULL,
    `lastname`                   varchar(255)       NOT NULL,
    `work_hours_per_week`        tinyint unsigned   NOT NULL DEFAULT 40
);

CREATE TABLE `role`
(
    `id`    varchar (50) PRIMARY KEY
);

CREATE TABLE `user_role`
(
    `user_id` int unsigned NOT NULL,
    `role_id` varchar (50) NOT NULL,
    PRIMARY KEY(user_id, role_id)
);


CREATE TABLE `password_reset_token`
(
    `id`            int unsigned    NOT NULL auto_increment PRIMARY KEY,
    `user_id`       int unsigned    NOT NULL,
-- sha256 are 64 chars in hex, so ascii encoding is enough
    `token_hash`    char(64)        CHARACTER SET ascii NOT NULL,
    `created`       datetime        DEFAULT CURRENT_TIMESTAMP
);


CREATE TABLE `task`
(
    `id`                            int unsigned             NOT NULL auto_increment PRIMARY KEY,
    `name`                          varchar(255)             NOT NULL,
    `scheduled_start`               datetime                 NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `scheduled_end`                 datetime                 NOT NULL,
    `scheduled_break_time`          time                     NOT NULL DEFAULT '01:00:00',
    `customer_name`                 varchar(255)             NOT NULL,
    `customer_description`          text(20000)              NOT NULL,
    `task_description`              text(20000)              NOT NULL
);


CREATE TABLE `user_task`
(
    `user_id`                       int unsigned             NOT NULL,
    `task_id`                       int unsigned             NOT NULL,
    `actual_start`                  datetime                 DEFAULT CURRENT_TIMESTAMP,
    `actual_end`                    datetime                 ,
    `actual_break_time`             time                     DEFAULT '01:00:00',
    `work_protocol`                 text(20000)              ,
    PRIMARY KEY(user_id, task_id)
);


ALTER TABLE `user` ADD UNIQUE (email);
ALTER TABLE `user` ADD UNIQUE (username);

ALTER TABLE `user_role` ADD CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ;
ALTER TABLE `user_role` ADD CONSTRAINT FOREIGN KEY (role_id) REFERENCES role(id) ON DELETE CASCADE ;

ALTER TABLE `password_reset_token` ADD CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ;
ALTER TABLE `password_reset_token` ADD UNIQUE (token_hash);

ALTER TABLE `user_task` ADD CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE ;
ALTER TABLE `user_task` ADD CONSTRAINT FOREIGN KEY (task_id) REFERENCES task(id) ON DELETE CASCADE ;
